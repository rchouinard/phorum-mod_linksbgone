<?php

if (!defined("PHORUM_ADMIN")) return;

$error = "";
if (filter_has_var(INPUT_POST, "mod") && filter_input(INPUT_POST, "mod", FILTER_SANITIZE_STRING) == "linksbgone") {
    $settings = array (
        "check_reg" => filter_input(INPUT_POST, "check_reg", FILTER_SANITIZE_NUMBER_INT),
        "min_age"   => filter_input(INPUT_POST, "min_age",   FILTER_SANITIZE_NUMBER_INT),
        "min_posts" => filter_input(INPUT_POST, "min_posts", FILTER_SANITIZE_NUMBER_INT),
    );

    if(!phorum_db_update_settings(array ("linksbgone" => $settings))) {
        $error="Database error while updating settings";
    } else {
        $PHORUM["linksbgone"] = $settings;
        phorum_admin_okmsg("Links B Gone Settings Saved");
    }
}

if (strlen($error) !== 0) {
    phorum_admin_error($error);
}

// Display the settings form
include_once "include/admin/PhorumInputForm.php";

$frm = new PhorumInputForm("", "post", "Save");
$frm->hidden("module", "modsettings");
$frm->hidden("mod", "linksbgone");
$frm->addrow("Check Registered Users", $frm->select_tag("check_reg", array ("No", "Yes"), $PHORUM["linksbgone"]["check_reg"]));
$frm->addrow("Skip users older than this many days:", $frm->text_box("min_age", $PHORUM["linksbgone"]["min_age"], 7));
$frm->addrow("Skip users with more than this many posts:", $frm->text_box("min_posts", $PHORUM["linksbgone"]["min_posts"], 10));
$frm->show();
