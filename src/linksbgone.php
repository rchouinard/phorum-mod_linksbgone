<?php

/**
 * @param string $message
 * @return string
 */
function phorum_linksbgone_check_links($message)
{
    global $PHORUM;

    // If the user is an admin or the post is a modpost, return
    if ($PHORUM["user"]["admin"] || $message["moderator_post"]) {
        return $message;
    }

    // If user is registered and we're not checking registered users, return
    if ($PHORUM["user"]["user_id"] > 0 && !$PHORUM["linksbgone"]["check_reg"]) {
        return $message;
    }

    // If the user's account is older than our threshold, return
    $accountAge = (mktime() - (int) $PHORUM["user"]["date_added"]) / 60 / 60 /24;
    if ($accountAge > $PHORUM["linksbgone"]["min_age"]) {
        return $message;
    }

    // If the user has more posts than our threshold, return
    if ($PHORUM["user"]["posts"] > $PHORUM["linksbgone"]["min_posts"]) {
        return $message;
    }

    // Look for URLs in the message and quarantine if any are found
    // Regex taken from bbcode module
    $regex = "/((http|https|ftp):\/\/[a-z0-9;\/\?:@=\&\$\-_\.\+!*'\(\),~%#]+)/i";
    if (preg_match($regex, $message["body"]) > 0) {
        $message["status"] = PHORUM_STATUS_HOLD;
    }

    return $message;
}
